﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject star, superStar, astroid, missile, barrage;
    public float starSpeed, superSpeed, astroidSpeed, missileSpeed, barrageSpeed;
    public GameObject warShip;

    private void Start()
    {
        InvokeRepeating("StarSpawner", 0, starSpeed);
        // InvokeRepeating("SuperStarSpawner", 0, superSpeed);
        InvokeRepeating("AstroidSpawner", 0, astroidSpeed);
        InvokeRepeating("MissileSpawner", 0, missileSpeed);
        InvokeRepeating("BarrageSpawner", 0, barrageSpeed);
    }

    private void Update()
    {
        // Stops objects from spawing after player death
        if (Collisions.dead)
        {
            CancelInvoke();
        }

        // Spawns boss after smaller ships have been destroyed
        if (Enemy.boss >= 4)
        {
            Enemy.boss = 0;
            Instantiate(warShip, transform.position, transform.rotation);
        }
    }

    void StarSpawner()
    {
        float x = Camera.main.orthographicSize + 2;
        float y = Camera.main.orthographicSize * Camera.main.aspect + 2;

        Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
        target.z = 0;

        Instantiate(star, target, Quaternion.identity);
    }

    void SuperStarSpawner()
    {
        float x = Camera.main.orthographicSize + 2;
        float y = Camera.main.orthographicSize * Camera.main.aspect + 2;

        Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
        target.z = 0;

        Instantiate(superStar, target, Quaternion.identity);
    }

    void AstroidSpawner()
    {
        float x = Camera.main.orthographicSize + 2;
        float y = Camera.main.orthographicSize * Camera.main.aspect + 2;

        Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
        target.z = 0;

        Instantiate(astroid, target, Quaternion.identity);
    }

    void MissileSpawner()
    {
        float x = Camera.main.orthographicSize + 2;
        float y = Camera.main.orthographicSize * Camera.main.aspect + 2;

        Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
        target.z = 0;

        Instantiate(missile, target, Quaternion.identity);
    }
    void BarrageSpawner()
    {
        float x = Camera.main.orthographicSize + 2;
        float y = Camera.main.orthographicSize * Camera.main.aspect + 2;

        Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 0));
        target.z = 0;

        Instantiate(barrage, target, Quaternion.identity);
    }
}

//int x = Random.Range(0, Camera.main.pixelWidth);
//int y = Random.Range(0, Camera.main.pixelHeight);
