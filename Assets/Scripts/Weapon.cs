﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform firepoint;
    public GameObject bulletPrefab;
    public bool firing = false;
    public float timer;

    void Update()
    {
        // Detects if left click is pressed
        if (Input.GetMouseButtonDown(0))
        {
            firing = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            firing = false;
        }

        // Creates bullets 
        if (firing && Time.time > timer && Manager.fuel >= 2)
        {
            timer = Time.time + Manager.fireRate; //bullet firerate
            Manager.fuel -= 2;
            Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
        }
    }
}
