﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    // Sets shield values
    public static float shield = 100f, maxShield = 100f;

    // Sets fuel values
    public static float fuel = 1000f, maxFuel = 1000f;

    public static float gems = 0f, depo = 0f, bank;

    // Weapon fire rate
    public static float fireRate = 0.3f;

    // Upgrade costs
    public static int shieldBuy = 5;
    public static int fuelBuy = 5;
    public static int gunBuy = 5;

    public static int shieldLevel = 1, fuelLevel = 1, gunLevel = 1;
    public GameObject warShip;

    public static bool dead = false;
    public GameObject Player;
    public GameObject deathEffect;

    public Image fuelBar, fuelBar2, shieldBar, shieldBar2;
    public TextMeshProUGUI gemCount, depoCount, moneyCount;

    private void Start()
    {
        // Resets values when game starts
        shield = maxShield;
        fuel = maxFuel;
        gems = 0f;
        depo = 0f;
        dead = false;
        Enemy.boss = 0;
    }
}
