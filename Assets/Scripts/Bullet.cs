﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 40f;
    public Rigidbody2D rb;
    public int damage = 50;
    private Transform target;

    void Start()
    {
        // Makes bullets move 
        rb.velocity = transform.right * speed;
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    void Update()
    {
        // Bullets despawn when far from player
        if (Vector2.Distance(transform.position, target.position) > 25)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Trigger")
        {
            Debug.Log("Shot " + collision.tag);

            Enemy enemy = collision.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.takeDamage(damage);
            }
            Destroy(gameObject);
        }
    }
}
