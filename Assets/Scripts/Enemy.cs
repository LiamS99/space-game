﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health = 100;
    public int reward = 10;
    public GameObject deathEffect;
    public GameObject gem;
    public int min = 1;
    public int max = 4;
    public static int boss = 0;
    public int bossPoint = 0;

    public void takeDamage (int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            boss += bossPoint;
            die();
        }
    }

    void die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);

        // Randomly spawns between min and max values
        for (int i = 0; i < Random.Range(min, max); i++)
        {
            Instantiate(gem, transform.position, Quaternion.identity);
        }
    }
}
