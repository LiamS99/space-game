﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Collisions : MonoBehaviour
{
    // Explosion effects
    public GameObject starEffect, deathEffect, enemyEffect, missileEffect, gemEffect;

    // User Interface
    public Image fuelBar, fuelBar2, shieldBar, shieldBar2;
    public TextMeshProUGUI gemCount, depoCount, moneyCount;

    public GameObject menu;
    public GameObject Player;
    public static bool dead = false;

    float RegenWait;

    private void Start()
    {
        // Resets values when game starts
        Manager.shield = Manager.maxShield;
        Manager.fuel = Manager.maxFuel;
        Manager.gems = 0f;
        Manager.depo = 0f;
        dead = false;
        Enemy.boss = 0;
    }

    private void Update()
    {
        // User Interface
        float fuelFill = Manager.fuel / Manager.maxFuel;
        fuelBar.fillAmount = fuelFill;
        fuelBar2.fillAmount = fuelFill;

        float shieldFill = Manager.shield / Manager.maxShield;
        shieldBar.fillAmount = shieldFill;
        shieldBar2.fillAmount = shieldFill;

        gemCount.text = "Gems: " + Manager.gems;
        depoCount.text = "Deposited: " + Manager.depo;
        moneyCount.text = "Bank: " + Manager.bank;

        // Player death
        if (Manager.shield <= 0)
        {
            dead = true;
            Destroy(Player);
            Instantiate(deathEffect, transform.position, Quaternion.identity);
            Manager.bank += Manager.depo;
        }

        // Shield regen
        if (Manager.shield < Manager.maxShield && Time.time > RegenWait)
        {
            Manager.shield += 1 * Time.deltaTime * 5;
        }

        // Enables or disables the main menu
        if (dead)
        {
            menu.active = true;
        }
        else
        {
            menu.active = false;
        }

        // Prevents extra shields and fuel
        if (Manager.shield > Manager.maxShield)
        {
            Manager.shield = Manager.maxShield;
        }

        if (Manager.fuel > Manager.maxFuel)
        {
            Manager.fuel = Manager.maxFuel;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Star collision
        if (collision.gameObject.tag == "Star")
        {
            Debug.Log("Star collision");
            Destroy(collision.gameObject);
            Instantiate(starEffect, transform.position, Quaternion.identity);
            FuelReward(15);
        }

        // Super Star collision
        if (collision.gameObject.tag == "Super Star")
        {
            Debug.Log("Super Star collision");
            Destroy(collision.gameObject);
            Instantiate(starEffect, transform.position, Quaternion.identity);
            FuelReward(50);
        }

        // Gem collision
        if (collision.gameObject.tag == "Gem")
        {
            Debug.Log("Gem collision");
            Destroy(collision.gameObject);
            Instantiate(gemEffect, transform.position, Quaternion.identity);
            GemReward(1);
        }

        // Astroid collision
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Enemy collision");
            ShieldDamage(10);

            // Astroids are destroyed with speed boost
            if (Movement.speed > 3)
            {
                ShieldDamage(20);
                Destroy(collision.gameObject);
                Instantiate(enemyEffect, transform.position, Quaternion.identity);
            }
        }

        // Blackhole collision
        if (collision.gameObject.tag == "Blackhole")
        {
            Debug.Log("Blackhole collision");
            ShieldDamage(10000);
            Instantiate(deathEffect, transform.position, Quaternion.identity);
        }

        // Missile collision
        if (collision.gameObject.tag == "Missile")
        {
            Debug.Log("Missile collision");
            ShieldDamage(15);
            Destroy(collision.gameObject);
            Instantiate(missileEffect, transform.position, Quaternion.identity);
        }

        // Enemy Ship collision
        if (collision.gameObject.tag == "Enemy Ship")
        {
            Debug.Log("Enemy Ship collision");
            ShieldDamage(50);
        }

        // Enemy bullet collisions
        if (collision.gameObject.tag == "Bullet")
        {
            Debug.Log("Enemy bullet collision");
            ShieldDamage(10);
        }

        // Border collisions
        if (collision.gameObject.tag == "Border")
        {
            Debug.Log("Border collision");
            ShieldDamage(100);
        }

        // Safe Zone refill
        if (collision.gameObject.tag == "Safe")
        {
            Debug.Log("Safe Zone collision");

            if (Manager.shield < Manager.maxShield)
            {
                Manager.shield += Time.deltaTime * 5;
            }

            if (Manager.fuel < Manager.maxFuel)
            {
                Manager.fuel += Time.deltaTime * 5;
            }

            Manager.depo += Manager.gems;
            Manager.gems = 0;
        }
    }

    public void ShieldDamage(int damage)
    {
        // Decreases shield value
        Manager.shield -= damage;
        RegenWait = Time.time + 10f;
    }

    public void FuelReward(int reward)
    {
        // Increases fuel value
        if (Manager.fuel < Manager.maxFuel)
        {
            Manager.fuel += reward;
        }
    }

    public static void GemReward(int reward)
    {
        // Increases gem value
        Manager.gems += reward;
        Debug.Log("Gems: " + Manager.gems);
    }
}