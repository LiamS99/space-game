# Space-Game
WebGL game developed with Unity.

Play here: https://liams99.github.io/Space-Game/

Controls:
- Use mouse to control the movement direction.
- Hold Left click to fire weapon.
- Hold Right click to use speed boost.

Objective:
- Collect stars (yellow diamonds) to increase fuel.
- Find gems (pink diamonds) and deposit them at safe zones (white hexagons) to save them.
- Use deposited gems to buy player upgrades.
- Avoid getting shot by enemies and flying into blackholes.
