﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour
{
    public int damage = 50;
    private Transform target;
    public Rigidbody2D rb;

    void Start()
    {
        // Random rotation
        transform.Rotate(0, 0, Random.Range(0, 360));

        // Makes gems move 
        rb.velocity = transform.right * Random.Range(6, 9);
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    void Update()
    {
        // Gems despawn when far from player
        if (Vector2.Distance(transform.position, target.position) > 25)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Shot " + collision.tag);
        Enemy enemy = collision.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.takeDamage(damage);
        }
        Destroy(gameObject);
    }
}