﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class UpgradesMenu : MonoBehaviour
{
    public TextMeshProUGUI shieldCount, fuelCount, gunCount, bankCount;
    public TextMeshProUGUI shieldCost, fuelCost, gunCost;

    public GameObject mainMenu;
    public GameObject upgradeMenu;
    public GameObject ui;

    // Navigation
    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void UpgradeMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    // Returns to in-game UI
    public void ReturnToMain()
    {
        ui.active = true;
        mainMenu.active = true;
        upgradeMenu.active = false;
    }

    public void Exit()
    {
        Application.Quit();
    }

    // Quality settings
    public void setQualityHigh()
    {
        QualitySettings.SetQualityLevel(4);
    }

    public void setQualityMed()
    {
        QualitySettings.SetQualityLevel(3);
    }

    public void setQualityLow()
    {
        QualitySettings.SetQualityLevel(2);
    }

    // Upgrade Menu
    public void ShieldUpgrade()
    {
        if (Manager.bank >= Manager.shieldBuy)
        {
            Manager.bank -= Manager.shieldBuy;
            Manager.shieldBuy += 5;
            Manager.shieldLevel += 1;

            // Increases max shield
            Manager.maxShield += 100;
        }
    }

    public void FuelUpgrade()
    {
        if (Manager.bank >= Manager.fuelBuy)
        {
            Manager.bank -= Manager.fuelBuy;
            Manager.fuelBuy += 5;
            Manager.fuelLevel += 1;

            // Increases max fuel
            Manager.maxFuel += 500;
        }
    }

    public void FireRateUpgrade()
    {
        if (Manager.bank >= Manager.gunBuy)
        {
            Manager.bank -= Manager.gunBuy;
            Manager.gunBuy += 5;
            Manager.gunLevel += 1;

            // Decreases the value of fireRate, which increases the in-game fire rate
            Manager.fireRate -= 0.05f;
        }
    }

    private void Update()
    {
        // Displays gems
        bankCount.text = "Bank: " + Manager.bank;

        // Updates title count
        shieldCount.text = "Shield Capacity " + Manager.shieldLevel;
        fuelCount.text = "Fuel Efficiency " + Manager.fuelLevel;
        gunCount.text = "Fire Rate " + Manager.gunLevel;

        // Updates cost
        shieldCost.text = "Cost: " + Manager.shieldBuy;
        fuelCost.text = "Cost: " + Manager.fuelBuy;
        gunCost.text = "Cost: " + Manager.gunBuy;
    }
}
