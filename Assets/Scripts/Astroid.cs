﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astroid : MonoBehaviour
{
    public int health = 100;
    public int reward = 10;
    public GameObject deathEffect;
    public GameObject gem;

    public void takeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            die();
        }
    }

    void die()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);

        // Randomly spawns between 1 to 3 gems
        for (int i = 0; i < Random.Range(1, 4); i++)
        {
            Instantiate(gem, transform.position, Quaternion.identity);
        }
    }
}
