﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawner : MonoBehaviour
{
    private Transform target;
    public float distance = 15f;

    void Start()
    {
        // Finds the player's location 
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    void Update()
    {
        // Destroys objects when too far from player
        if (Vector2.Distance(transform.position, target.position) > distance)
        {
            Destroy(gameObject);
        }
    }
}
