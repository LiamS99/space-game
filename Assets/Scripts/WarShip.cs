﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarShip : MonoBehaviour
{
    public static float speed = 7f;
    public float stop = 50;
    private Transform target;
    public Transform firepoint;
    public Transform firepoint2;
    public Transform firepoint3;
    public GameObject bulletPrefab;
    public float fireRate;

    void Start()
    {
        // Finds the player's location
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void Update()
    {
        // Rotates toward player
        RotateTowards(target.position);

        // Enemy stops when too close to player
        if (Vector2.Distance(transform.position, target.position) < 9)
        {
            speed = 0f;
        }

        // Move towards player within distance
        if (Vector2.Distance(transform.position, target.position) < 80 && Vector2.Distance(transform.position, target.position) > 9)
        {
            speed = 7f;
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        // Fires bullets when close to player
        if (Vector2.Distance(transform.position, target.position) < 30f && Time.time > fireRate)
        {
            fireRate = Time.time + 0.4f; //bullet firerate
            Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
            Instantiate(bulletPrefab, firepoint2.position, firepoint2.rotation);
            Instantiate(bulletPrefab, firepoint3.position, firepoint3.rotation);
        }
    }

    private void RotateTowards(Vector2 target)
    {
        Vector2 direction = target - (Vector2)transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion qTo = Quaternion.Euler(Vector3.forward * angle);

        // Slows the rotation
        transform.rotation = Quaternion.RotateTowards(transform.rotation, qTo, 50 * Time.deltaTime);
    }
}
