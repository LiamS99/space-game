﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public static float speed = 3f;
    public bool speedBoost = false;
    public float discharge;

    private void Start()
    {
        speed = 3f;
        speedBoost = false;
}

    private void Update()
    {
        // Player can only change speeds when alive
        if (Collisions.dead == false)
        {

            // Decreases movement speed
            if (Input.GetMouseButtonDown(0))
            {
                speed = 1.5f;
                Camera.main.orthographicSize = 6;
                speedBoost = false;
            }

            // Resets movement speed
            if (Input.GetMouseButtonUp(0))
            {
                speed = 3f;
                Camera.main.orthographicSize = 6;
            }

            // Increases movement speed and camera size
            if (Input.GetMouseButtonDown(1) && Manager.fuel >= 50)
            {
                speed = 50f;
                Camera.main.orthographicSize = 9;
                speedBoost = true;
            }

            // Resets movement speed
            if (Input.GetMouseButtonUp(1))
            {
                speed = 3f;
                Camera.main.orthographicSize = 6;
                speedBoost = false;
            }

            // Decreases fuel over time while using speed boost
            if (speedBoost && Time.time > discharge)
            {
                discharge = Time.time + 0.1f;
                Manager.fuel -= 400 * Time.deltaTime;
            }
        }

        // Object follows mouse 
        Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        target.z = transform.position.z;

        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        // Movement while player is dead
        if (Collisions.dead)
        {
            speed = 1f;
            Camera.main.orthographicSize = 7;
        }
    }
}
